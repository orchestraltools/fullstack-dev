const express = require("express");
const bodyParser = require("body-parser");
const instruments = require("./instruments.json");

const app = express();
app.use(bodyParser.json({ type: "application/json" }));

const port = process.env.PORT;

// initialise likes for each instrument - see top level README.md
const likes = instruments.reduce(
  (all, instrument) => ({ ...all, [instrument.id]: 0 }),
  {}
);

app.get("/api/instruments", (req, res) => {
  res.json(instruments);
});

app.get("/api/instruments/:instrumentId/likes", (req, res) => {
  const { instrumentId } = req.params;
  return res.json(likes[instrumentId]);
});

app.patch("/api/instruments/:instrumentId/likes", (req, res) => {
  const { instrumentId } = req.params;
  const { delta } = req.body;
  likes[instrumentId] += delta;
  return res.json(likes[instrumentId]);
});

app.listen(port, () => {
  console.log(`Backend listening on local port ${port}`);
});
