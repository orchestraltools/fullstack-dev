import { useState, useEffect, useCallback } from "react";

import Instrument from "./Instrument";
import { fetchData, updateLike } from "./logic";

import "./App.css";

function App() {
  const [instruments, setInstruments] = useState([]);

  useEffect(() => {
    fetchData().then((instruments) => setInstruments(instruments));
  }, [setInstruments]);

  const onLike = useCallback(
    async (ev) => {
      const instrumentId = ev.target.id;
      const instrument = instruments.find(
        (instrument) => instrument.id === instrumentId
      );
      const updatedInstrument = await updateLike(instrument, +1);
      setInstruments(
        instruments.map((instrument) =>
          instrument.id === instrumentId ? updatedInstrument : instrument
        )
      );
    },
    [instruments]
  );

  return (
    <div className="App">
      <h1>All instruments</h1>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: 10,
          margin: 10,
        }}
      >
        {instruments.map((values) => (
          <Instrument key={values.id} {...values} onLike={onLike} />
        ))}
      </div>
    </div>
  );
}

export default App;
