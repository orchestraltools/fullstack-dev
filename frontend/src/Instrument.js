export default function Instrument({
  id,
  name,
  description,
  likes,
  image,
  onLike,
}) {
  return (
    <div
      style={{
        border: "solid 1px lightgray",
        padding: 10,
        width: "70%",
        margin: "auto",
        borderRadius: 5,
      }}
    >
      <h2 style={{ textTransform: "capitalize" }}>{name}</h2>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <img alt="instrument" height={150} src={image} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            textAlign: "left",
            padding: "0px 10px",
          }}
        >
          <div>{description}</div>

          <div style={{ textAlign: "right", marginTop: 10 }}>
            <button id={id} style={{ fontSize: 16 }} onClick={onLike}>
              ❤️
            </button>
            <span style={{ fontSize: 12, marginLeft: 3 }}>{likes}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
