const baseUrl = "/api";

export async function fetchData() {
  const instruments = await fetch(`${baseUrl}/instruments`).then((res) =>
    res.json()
  );

  const likes = await Promise.all(
    instruments.map(({ id }) =>
      fetch(`${baseUrl}/instruments/${id}/likes`)
        .then((res) => (res.ok ? res.json() : []))
        .then((likes) => ({ id, likes }))
    )
  );

  return instruments.reduce(
    (acc, instrument, idx) => [
      ...acc,
      { ...instrument, likes: likes[idx].likes },
    ],
    []
  );
}

export async function updateLike(instrument, delta) {
  const newLikes = await fetch(
    `${baseUrl}/instruments/${instrument.id}/likes`,
    {
      method: "PATCH",
      body: JSON.stringify({ delta }),
      headers: {
        "content-type": "application/json",
      },
    }
  ).then((res) => res.json());

  return { ...instrument, likes: newLikes };
}
