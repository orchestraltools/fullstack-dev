# Fullstack Coding Test

## Start the test setup

You should have `docker` and `docker-compose` installed on your dev-machine.
After cloning the repository type on the root level of the repository:

```
docker-compose up
```

_Note: The commend on newer Apple Silicon Macs is `docker compose up`_

Once everything is running, you can access the test setup at location: `http://localhost`

_Note: it might take a while, until the frontend dev-server is up and running._

_Further note: setup is running on local port 80, so you might need to run docker with according rights. Alternatively change the gateway port in the `docker-compose.yml`.
For Example access via `http://localhost:3000` by replacing `"80:80"` with `"3000:80"` on line 30._

## Make yourself familiar with the local setup

The local docker infrastructure consists of:

- gateway: unified access to backend and frontend services
- frontend: not much more than a create-react-app setup
- backend: small, incomplete and draftish REST-API based on express

_Frontend code changes are directly applied when shown in a browser. Changes to the backend code require manual restart of the service, eg. `docker-compose restart backend`_

## What's the scope

The Webapp lists (for this use case) a hardcoded number of dummy instruments. You can add _likes_ by clicking the heart button. Next to the button, the current numer of likes is displayed.

Likes are accumulated in the backend service. At the moment, their values are kept in-memory.

The focus of the coding test, is considering a better persistence strategy. And if possible sketch this directly in the setup. Feel free to make any changes to the existing services (back- and frontend) as well as introducing new services to the docker setup. Apply any changes that help you focusing on an improved, consistent experience.

The following questions might help guiding your solution:

- How should these we persist likes instead of using an in-memory storage? What problems do you forsee with the in-memory solution?

- Imagine we would want to scale-up number of backend services. For exampoe to balance the load or provide dedicated access to different regions. How could we keep count consistent?

- How could we improve the API on the backend? What issues do you see with the current API?

- Did you try to artificially slowdown the backend services endpoints to simulate stress? How does the solution feel in this situation?

- What else would you want to improve on the existing services and the way they communicatee. Also considering the value of the "like-feature" from less technical product-owner perspective?

- There are certainly things that you dislike in the given boilerplate. What are they? Let's talk about them in the follow-up interview

## What's next

Please don't commit to the public repository. Instead you can:

- zip up you solution (omitting the `node_modules folders), or
- push your solution to a private repo somewhere under your control and get in contact for making an invitation
